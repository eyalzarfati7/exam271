import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
private url = "https://x576ooyx14.execute-api.us-east-1.amazonaws.com/beta"

predict(grade: number, psyc: number, payment:boolean ):Observable<any>{
  let json = {
    "data": 
      {
        "grade": grade,
        "psyc": psyc,
        "payment":payment
      }
  }
  let body  = JSON.stringify(json);
  return this.http.post<any>(this.url,body).pipe(
    map(res => {
      return res.body;       
    })
  );      
}



  constructor(private http:HttpClient) { }
}
