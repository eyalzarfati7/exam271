import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictService } from '../predict.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  userId:string; 
  students:Student[];
  students$; 
 
  constructor(private studentsService:StudentsService, public authService:AuthService,
    private predictService:PredictService) { }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId,id); 
  }

  public predict(student:Student){
    this.predictService.predict(student.grade,student.psyc,student.payment).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          student.drop='will drop';
          student.EZ=1;
          console.log(student.EZ);
        } else {
          console.log('no');
          student.drop='will not drop';
          student.EZ=1;
        }
          console.log(student.drop);
        
      }
    )
  }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.students$ = this.studentsService.getStudents(this.userId); 
        
        this.students$.subscribe(
          docs =>{
            console.log('init worked');        
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id; 
              this.students.push(student); 
            }
          }
        ) 
      }
    )

  }


}
