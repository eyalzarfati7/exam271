export interface Student {
    id:string,
    grade: number,
    psyc: number,
    payment:boolean,
    drop?: string,
    EZ?: number        
}
