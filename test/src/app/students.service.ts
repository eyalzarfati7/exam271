import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 



  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`, 
    ref => ref.orderBy('grade', 'asc')); 
    return this.studentCollection.snapshotChanges()      
  } 

  deleteStudent(Userid:string, id:string){
    this.db.doc(`users/${Userid}/students/${id}`).delete(); 
  } 
  addStudent(name:string,math:number,sat:number,payment:boolean){
    const student = {name:name,math:math,sat:sat,payment:payment}; 
    this.studentCollection.add(student);
  }


  updateStudent(id:string,name:string,math:number,sat:number,payment:boolean){
    this.db.doc(`/students/${id}`).update(
      {
        name:name,
        math:math,
        sat:sat,
        payment:payment

      }
    )
  }
  
  constructor(private db:AngularFirestore) { }
}
