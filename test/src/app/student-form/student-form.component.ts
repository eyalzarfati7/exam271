import { Router } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentsService } from '../students.service';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

 
  @Input() name:string;  
  @Input() grade:number;
  @Input() psyc:number;  
  @Input() payment:boolean;
  @Input() id:string; 

  onSubmit(){

  }
  save(){
    this.router.navigate(['/students'])
  }

  constructor(private router:Router ,private studentsService:StudentsService, public authService:AuthService, private db:AngularFirestore
    ) { }

  ngOnInit(): void {
  }

}
