// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBJbUG-C5K-eQR3KZan5vLM-LZMM1jFQFI",
    authDomain: "test-f716e.firebaseapp.com",
    projectId: "test-f716e",
    storageBucket: "test-f716e.appspot.com",
    messagingSenderId: "191032608929",
    appId: "1:191032608929:web:106130dbeeaad7438208e0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
