import { PredictService } from './../predict.service';
import { Customers } from './../interfaces/customers';
import { AuthService } from './../auth.service';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId:string;
  customers;
  customers$;
  text:string;

  addCustomerFormOPen = false;
  panelOpenState = false;
  constructor( private customersService:CustomersService, public authService:AuthService) { }

  add(customer:Customers){
    this.customersService.addCustomer(this.userId,customer.name, customer.yearsOfStudy,
      customer.income); 
  }


  public deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id);
  }
  update(customer:Customers){
    this.customersService.updateCustomer(this.userId,customer.id ,
      customer.name, customer.yearsOfStudy,customer.income);
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.customers$ = this.customersService.getCustomers(this.userId);
      }
    )

  }

}
