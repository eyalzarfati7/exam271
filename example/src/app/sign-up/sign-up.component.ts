import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})


export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  
  onSubmit(){
    this.auth.SignUp(this.email,this.password);
  }

  constructor(private auth:AuthService) { }
  
  ngOnInit(): void {
  }

}
