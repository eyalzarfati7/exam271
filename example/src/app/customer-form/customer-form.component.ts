import { Customers } from './../interfaces/customers';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})


export class CustomerFormComponent implements OnInit {
  
  @Input() name:string;
  @Input() yearsOfStudy:number;
  @Input() income:number;
  @Input() id:string; 
  @Output() update = new EventEmitter<Customers>();
  
  updateParent(){
    let customer:Customers = {id:this.id,name:this.name,yearsOfStudy:this.yearsOfStudy,
    income:this.income};
    this.update.emit(customer); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}
