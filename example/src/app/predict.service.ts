import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://x576ooyx14.execute-api.us-east-1.amazonaws.com/beta";

  private propablity:object = {};

  categories:object = {0:'Business', 1:'Entertainment', 2:'Politics', 3:'Sport', 4:'Tech'};

  classify(text:string){
    let json = {'articles':[
      {'text':text}
    ]}
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        console.log(res); 
      })
    )
  }



  constructor(private http:HttpClient) { }
}
