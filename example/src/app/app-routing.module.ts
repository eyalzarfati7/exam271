import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers/customers.component';
import { GreetingsComponent } from './greetings/greetings.component';
import { ItemComponent } from './item/item.component';
import { ItemsComponent } from './items/items.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignUpComponent},
  { path: 'customers', component: CustomersComponent},
  { path: 'items', component: ItemsComponent},
  { path: 'notfound', component: NotFoundComponent},
  { path: 'item', component: ItemComponent},
  { path: 'greetings', component: GreetingsComponent},
  { path: "**",redirectTo:"notfound"},
 
 
 
  
 
 
 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
