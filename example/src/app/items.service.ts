import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ItemsService {


 
  itemCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  change(userId:string,id:string,name:string,price:number,inventory:boolean,ST=2,change:true){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST,
        change:change
      }
    )
  }
  
  save(userId:string,id:string,name:string,price:number,inventory:boolean,ST=2){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }
  unsave(userId:string,id:string,name:string,price:number,inventory:boolean,ST=0){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }
  public getItems(userId){
    this.itemCollection = this.db.collection(`users/${userId}/items`, 
    ref => ref.orderBy('name', 'asc')); 
    return this.itemCollection.snapshotChanges()      
  } 

  deleteItem(Userid:string, id:string){
    this.db.doc(`users/${Userid}/items/${id}`).delete(); 
  } 

  addItem(userId:string,name:string,inventory:boolean, price:number){
    const item = {name:name, inventory:inventory,price:price }; 
    this.userCollection.doc(userId).collection('items').add(item);
  }

  updateItem(userId:string,id:string,name:string,inventory:boolean, price:number){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        inventory:inventory,
        price:price
      }
    )
  }

  constructor(private db:AngularFirestore) { }
}
