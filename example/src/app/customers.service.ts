import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

/*
customers = [{name:'eyal',yearsOfStudy:12,income:1000},{name:'sagi',yearsOfStudy:25,income:3000}];

public getCustomers(){
  const customerObservable = new Observable(observer =>{
    setInterval(()=> observer.next(this.customers),2000)
  });
  return customerObservable;
}

public getCustomers(){
  return this.customers;
}
*/

customerCollection:AngularFirestoreCollection;   
userCollection:AngularFirestoreCollection = this.db.collection('users');

public getCustomers(userId){
  this.customerCollection = this.db.collection(`users/${userId}/customers`);
  return this.customerCollection.snapshotChanges().pipe(map(
    collection =>collection.map(
      document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        console.log(data.id);
        return data;
      }
    )
  ))
}
addCustomer(userId:string,name:string, yearsOfStudy:number,income:number){
  const customer = {name:name, yearsOfStudy:yearsOfStudy, income:income}; 
  this.userCollection.doc(userId).collection('customers').add(customer);
}

public deleteCustomer(userId:string , id:string){
  this.db.doc(`users/${userId}/customers/${id}`).delete();
}

updateCustomer(userId:string,id:string,name:string,yearsOfStudy:number,income:number){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      name:name,
      yearsOfStudy:yearsOfStudy,
      income:income
    }
  )
}

constructor(private db:AngularFirestore) { }



}