export interface Customers {
    id:string,
    name:string,
    yearsOfStudy: number,
    income: number,
}
