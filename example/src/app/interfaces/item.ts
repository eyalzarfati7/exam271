export interface Item {
    id:string,
    name: string,
    price: number,
    inventory:boolean,
    ST?:number,
    change?:number,
    
}
