import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})

export class GreetingsComponent implements OnInit {

  userId;
  email;
  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
        console.log(this.userId);
      }
        )
  }
  
  
}