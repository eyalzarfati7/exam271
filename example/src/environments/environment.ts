// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBSD1bjddyk1R28Zhs1diGkNAxVFZnfa5w",
    authDomain: "example-test-cfaa3.firebaseapp.com",
    projectId: "example-test-cfaa3",
    storageBucket: "example-test-cfaa3.appspot.com",
    messagingSenderId: "330287808062",
    appId: "1:330287808062:web:6781f1b2b66a355ed59b6f"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
